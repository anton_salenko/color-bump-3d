﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchEventSystemMediator : BaseMediator
{

    [Inject] public TouchEventSystemView view { get; set; }

    public override void OnRegister()
    {
        
    }

    public override void OnRemove()
    {
        
    }
}
